#!/bin/bash
set -e

# setup gazebo environment
source "/usr/share/gazebo/setup.sh"

# exporting to bashrc
echo "source /usr/share/gazebo/setup.sh" >> ~/.bashrc

#GAZEBO_MODEL_PATH=$GAZEBO_MODEL_PATH:/dolly_ws/install/dolly_gazebo/share/dolly_gazebo/models/
#GAZEBO_RESOURCE_PATH=$GAZEBO_RESOURCE_PATH:/dolly_ws/install/dolly_gazebo/share/dolly_gazebo/worlds/

exec "$@"
