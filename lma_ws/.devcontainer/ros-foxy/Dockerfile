###################################################################################################################################################
# DEVELOPMENT STAGE
FROM nsdn/ros-foxy-lma:1.0 as ros-dev
LABEL stage="dev-stage"

### Docker inside Docker configuration
# Install zsh and Oh-My-zsh
ARG INSTALL_ZSH="true"
ARG INSTALL_OH_MYS="true"

# Change User name
ARG USER_NAME=ros-lma

# User UID and GID
ARG USER_UID=1000
ARG USER_GID=$USER_UID

# Upgrade OS packages to their latest versions
ARG UPGRADE_PACKAGES="true"

# Enable non-root Docker access in container
ARG ENABLE_NONROOT_DOCKER="true"

# Install needed packages and setup non-root user. 
ARG SOURCE_SOCKET=/var/run/docker-host.sock
ARG TARGET_SOCKET=/var/run/docker.sock

COPY library-scripts/*.sh /tmp/library-scripts/

RUN apt-get update \
    && /bin/bash /tmp/library-scripts/common-debian.sh "${INSTALL_ZSH}" "${USER_NAME}" "${USER_UID}" "${USER_GID}" "${UPGRADE_PACKAGES}" \
    # Use Docker script from script library to set things up
    && /bin/bash /tmp/library-scripts/docker-debian.sh "${ENABLE_NONROOT_DOCKER}" "${SOURCE_SOCKET}" "${TARGET_SOCKET}" "${USERNAME}" \
    # Clean up
    && apt-get autoremove -y && apt-get clean -y && rm -rf /var/lib/apt/lists/* /tmp/library-scripts/

# Packages that have been installed
# PACKAGE_LIST="
#     apt-utils \
#     git \
#     openssh-client \
#     gnupg2 \
#     iproute2 \
#     procps \
#     lsof \
#     htop \
#     net-tools \
#     psmisc \
#     curl \
#     wget \
#     rsync \
#     ca-certificates \
#     unzip \
#     zip \
#     nano \
#     vim-tiny \
#     less \
#     jq \
#     lsb-release \
#     apt-transport-https \
#     dialog \
#     libc6 \
#     libgcc1 \
#     libgssapi-krb5-2 \
#     libicu[0-9][0-9] \
#     liblttng-ust0 \
#     libstdc++6 \
#     zlib1g \
#     locales \
#     sudo \
#     ncdu \
#     man-db"

# # Use this RUN statement to add your own dependencies and packages.
# RUN apt-get update && apt-get install -y \
#     ros-foxy-turtlesim \
#     && rm -rf /var/lib/apt/lists/*

COPY ./lma_ros_entrypoint.sh /
COPY ./library-scripts/docker-init.sh /usr/local/share

ENTRYPOINT [ "/lma_ros_entrypoint.sh" ]
CMD [ "bash" ]

###################################################################################################################################################
# DEVELOPMENT STAGE
FROM ros-dev as vscode-dev
LABEL stage="vscode-stage"

RUN apt-get update && apt-get install -y \
    gpg \
    snapd \
    software-properties-common \
    && rm -rf /var/lib/apt/lists/*

RUN wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | apt-key add - \
    && add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"

RUN apt-get -y update && apt-get install -y \
    code \
    libxtst-dev \
    && rm -rf /var/lib/apt/lists/*

COPY ./lma_ros_entrypoint.sh /
COPY ./library-scripts/docker-init.sh /usr/local/share

ENTRYPOINT [ "/lma_ros_entrypoint.sh" ]
CMD [ "bash" ]