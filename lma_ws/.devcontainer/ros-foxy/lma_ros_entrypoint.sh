#!/bin/bash
set -e

source /usr/local/share/docker-init.sh

# setup project environment
source /opt/ros/foxy/setup.bash
source /usr/share/gazebo/setup.sh

# # exporting to bashrc
# echo "source /opt/ros/foxy/setup.bash" >> ~/.bashrc
# echo "source /usr/share/gazebo/setup.sh" >> ~/.bashrc
# echo "export ROS_DOMAIN_ID=200" >> ~/.bashrc

exec "$@"